<p align="center"><a href="https://mushmero.com" target="_blank"><img src="https://bitbucket.org/mushmero/lapdash/raw/1d4851491534123e75988bd6b23fa81f6121ae95/src/resources/assets/images/lapdash.png" width="100" alt="LapDash Logo"></a></p>

<p align="center">
<a href="https://bitbucket.org/mushmero/lapdash/issues?status=new&status=open"><img src="https://img.shields.io/bitbucket/issues/mushmero/lapdash?style=for-the-badge" alt="Issues"></a>
<a href="#"><img src="https://img.shields.io/bitbucket/pr/mushmero/lapdash?style=for-the-badge" alt="Pull Request"></a>
<a href="#"><img src="https://img.shields.io/badge/License-MIT-blue.svg?style=for-the-badge" alt="MIT License"></a>
</p>

## About LAPDash
LapDash is shortform for Laravel Admin Panel Dashbord which is an admin panel dashboard scaffold initial build on top of [Laravel 9](https://laravel.com), [AdminLTE Ui](https://github.com/jeroennoten/Laravel-AdminLTE) & [Bootstrap 4](https://getbootstrap.com/docs/4.6/getting-started/introduction/). It is a powerful scaffold to start build your MVP application as it already consist User Module, Roles Module, Permissions Module and Activity Logs Module. Now support Laravel 10.

Some of the features exist in this repository are

- Auto Logout when inactive
- Detailed activity logging
- Powerfull Roles & Permissions module
- Fully Customizable

Use the package with laravel and start build your mvp apps

## Pre-requisites

- Lapdash require vite to work
- Install package vite & laravel-vite-plugin
- add/replace vite.config.js as below

~~~js
import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/sass/app.scss',
                'resources/js/app.js',
            ],
            refresh: true,
        }),
    ],
});
~~~

- make sure package.json has been update to dev : vite & prod : vite build
- for Laravel 10 can ignore above steps as vite come as default in Laravel 10

## Installation

- composer require mushmero/lapdash
- add Spatie\Permission\PermissionServiceProvider::class in config/app
- add Mushmero\Lapdash\LapdashServiceProvider::class in config/app
- php artisan lapdash:initialize
- php artisan appversion:sync --major=? --minor=? --build=? (replace ? with your version)
- npm install
- npm run dev for development
- npm run prod for production
- use php artisan lapdash:db-setup for project deployment

### Default Login

~~~txt
Username: admin@lapdash.com
Password: Sup3r@dm!n
~~~

## Information

- Initialize command can be run only once.
- For new fresh project deployment.
- Not for existing project.

## License

LapDash is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).