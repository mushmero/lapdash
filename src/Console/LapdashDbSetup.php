<?php

namespace Mushmero\Lapdash\Console;

use File;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Mushmero\Lapdash\Models\User;

class LapdashDbSetup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lapdash:db-setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'DB Setup for initialize deployment';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $isRollback = $this->option('rollback');
        if(!$isRollback){
            $this->info('initialize DB Setup');
            $this->envCreateIfNotExist();
            $this->info('DB Migrations');
            $this->call('migrate');
            $this->info('Generate all permissions');
            $this->call('permissions:generate');
            $this->info('Create default role Superadmin');
            $this->call('roles:generate',['rolename' => 'Superadmin']);
            if(User::where('email', 'admin@lapdash.com')->count() == 0){        
                $this->info('Seed default admin user');
                Artisan::call('db:seed AdminUserSeeder');
            }else{
                $this->info('User admin@lapdash.com already exist');
            }
            $this->info('Sync all permissions to admin');
            $this->call('permissions:syncadmin');
            $this->info('End of DB setup');
        }else{
            $this->info('Rollback Setup');
            Artisan::call('db:wipe');
            $this->info('Rollback Success');

        }
        return Command::SUCCESS;
    }

    private function envCreateIfNotExist()
    {
        if(!File::exists(base_path('.env'))){
            $this->info('Copy .env.example to .env');
            copy('.env.example', '.env');
            $this->info('generate application key');
            $this->call('key:generate');
        }
        return true;
    }
}
