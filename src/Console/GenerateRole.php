<?php

namespace Mushmero\Lapdash\Console;

use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;

class GenerateRole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'roles:generate {rolename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate role {rolename}';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $rolename = $this->argument('rolename');

        $roleExist = Role::where(['name' => $rolename])->first();
        if(!$roleExist){
            Role::create(['name' => $rolename]);
            $this->info('Create role '.$rolename);
        }else{
            $this->warn('Role '.$rolename.' already exist. Please check.');
        }

        return Command::SUCCESS;
    }
}
