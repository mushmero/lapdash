<?php

namespace Mushmero\Lapdash\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Permission;

class GenerateRoutePermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permissions:generate {route?*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate permission based on route name';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $arg = $this->arguments();
        $routelist = $arg['route'];
        
        if(!empty($routelist)){
            foreach($routelist as $single){
                $this->info('Route :'.$single);
                $permission = Permission::where('name', $single)->first();

                if (is_null($permission)) {
                    $this->info('Register route '.$single);
                    Permission::create(['name' => $single]);
                }
            }
        }else{
            $routes = Route::getRoutes()->getRoutes();
    
            foreach ($routes as $route) {
                $this->info($route->getName());
                if ($route->getName() != '' && $route->getAction()['middleware']['0'] == 'web') {
                    $permission = Permission::where('name', $route->getName())->first();
    
                    if (is_null($permission)) {
                        $this->info('register route '.$route->getName());
                        Permission::create(['name' => $route->getName()]);
                    }
                }
            }
        }

        $this->info('Permission routes added successfully.');

        return Command::SUCCESS;
    }
}
