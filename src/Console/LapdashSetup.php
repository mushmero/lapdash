<?php

namespace Mushmero\Lapdash\Console;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;
use Mushmero\Lapdash\Console\PackageResources\AuthViewsResource;

class LapdashSetup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lapdash:initialize';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate default permissions, generate superadmin and assign role to superadmin user. This command should only run first time only';

    protected $requiredPlugins;

    protected $auth_views;

    public function __construct()
    {
        parent::__construct();
        $this->requiredPlugins = ['bootstrapSwitch','bsCustomFileInput','datatables','datatablesPlugins','icheckBootstrap','select2','sweetalert2','tempusdominusBootstrap4'];
        $this->auth_views = new AuthViewsResource();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (!$this->checkLogExists()){
            $logChannel = $this->buildCustomLog();
            $this->info('Initialize setup for lapdash');
            Log::stack([$logChannel])->info('Initialize setup for lapdash');

            $this->installRequiredPackage($logChannel);
            $this->installAdminlte($logChannel);
            $this->installRequiredPlugins($logChannel);
            $this->installLapdash($logChannel);
            
            $this->info('Initialize completed');
            Log::stack([$logChannel])->info('Initialize completed');
    
            return Command::SUCCESS;
        }else{
            $this->info('Command already run once');
            $this->info('You may run this command once / initial setup');
            return Command::FAILURE;
        }
    }

    private function buildCustomLog()
    {
        $channel = Log::build([
            'driver' => 'single',
            'path' => storage_path('logs/InitialSetup.log'),
        ]);

        return $channel;
    }

    private function checkLogExists()
    {
        $exists = file_exists(storage_path('logs/InitialSetup.log'));
        return $exists;
    }

    private function installAdminlte($logChannel)
    {    
        $this->info('Install default adminlte package');
        Log::stack([$logChannel])->info('Initialize adminlte:install command');
        try {
            // install adminlte
            $this->info('Command adminlte:install');
            $this->call('adminlte:install');
        } catch (\Throwable $th) {
            $this->error('Failed initialize adminlte:install command : '.$th->getMessage());
            Log::stack([$logChannel])->info('Failed initialize adminlte:install command : '.$th->getMessage());
        }

        Log::stack([$logChannel])->info('Initialize laravel ui bootstrap command');
        try {
            // install laravel ui
            $this->info('Command ui bootstrap --auth');
            Artisan::call('ui:auth --force');
            Artisan::call('ui bootstrap');
        } catch (\Throwable $th) {
            $this->error('Failed initialize laravel ui bootstrap command : '.$th->getMessage());
            Log::stack([$logChannel])->info('Failed initialize laravel ui bootstrap command : '.$th->getMessage());
        }

        Log::stack([$logChannel])->info('Initialize adminlte:install auth view command');
        try {
            // install lapdash auth view
            $this->info('Install lapdash auth views');
            // Artisan::call('adminlte:install --only=auth_views');
            $this->auth_views->install();
            $this->info('Replace route');
            $this->replaceRoute($logChannel);
        } catch (\Throwable $th) {
            $this->error('Failed Install lapdash auth views : '.$th->getMessage());
            Log::stack([$logChannel])->info('Failed Install lapdash auth views : '.$th->getMessage());
        }
        $this->info('End of install default adminlte package');
        
        $this->migrateDB($logChannel);

    }

    private function installRequiredPlugins($logChannel)
    {
        $this->info('Install required adminlte plugins');
        $plugins = $this->requiredPlugins;

        foreach($plugins as $plugin){
    
            Log::stack([$logChannel])->info('Initialize adminlte plugin '.$plugin.' install');
            try {
                // install plugin
                $this->info('Install plugin '.$plugin);
                Artisan::call('adminlte:plugins install --plugin='.$plugin);
            } catch (\Throwable $th) {
                $this->error('Failed initialize '.$plugin.' install : '.$th->getMessage());
                Log::stack([$logChannel])->info('Failed initialize '.$plugin.' install : '.$th->getMessage());
            }
        }
        $this->info('End of install adminlte plugins');
    }

    private function installLapdash($logChannel)
    {    
        $this->info('Install lapdash intial setup');
        Log::stack([$logChannel])->info('Initialize publish command');
        try {
            // publish
            $this->info('Publish commands');
            $this->call('vendor:publish', ['--tag' => 'admin-seeds','--force' => true]);
            $this->call('vendor:publish', ['--tag' => 'lapdash-user-models','--force' => true]);
            $this->call('vendor:publish', ['--tag' => 'lapdash-config','--force' => true]);
            $this->call('vendor:publish', ['--tag' => 'lapdash-assets','--force' => true]);
            $this->call('vendor:publish', ['--tag' => 'lapdash-env','--force' => true]);
            // $this->call('vendor:publish', ['--tag' => 'lapdash-home','--force' => true]);
            $this->call('vendor:publish', ['--tag' => 'lapdash-views','--force' => true]);
            $this->call('vendor:publish', ['--tag' => 'lapdash-stuffs','--force' => true]);
            $this->call('lapdash:rebuildenv');
        } catch (\Throwable $th) {
            $this->error('Failed initialize publish command : '.$th->getMessage());
            Log::stack([$logChannel])->info('Failed initialize publish command : '.$th->getMessage());
        }

        Log::stack([$logChannel])->info('Initialize permissions:generate command');
        $this->info('Generate permissions');
        $this->call('permissions:generate', ['route' => [
            'login',
            'logout',
            'register',
            'password.request',
            'password.email',
            'password.reset',
            'password.update',
            'password.confirm',
            'user_access',
            'user_access.show',
            'users',
            'users.store',
            'users.create',
            'users.edit',
            'users.update',
            'users.destroy',
            'users.show',
            'users.delete',
            'users.restore',
            'roles',
            'roles.store',
            'roles.create',
            'roles.edit',
            'roles.update',
            'roles.delete',
            'roles.show',
            'permissions',
            'permissions.store',
            'permissions.create',
            'permissions.edit',
            'permissions.update',
            'permissions.delete',
            'permissions.show',
            'home',
            'roles.select',
            'roles.select.store',
        ]]);
        $this->call('permissions:generate');

        Log::stack([$logChannel])->info('Initialize roles:generate command');
        $this->info('Generate role Superadmin');
        $this->call('roles:generate',['rolename' => 'Superadmin']);
        
        Log::stack([$logChannel])->info('Initialize db:seed AdminUserSeeder command');
        try {
            // create superadmin user and assign roles & permissions
            $this->info('Seed default admin user');
            Artisan::call('db:seed AdminUserSeeder');
        } catch (\Throwable $th) {
            $this->error('Failed initialize db:seed AdminUserSeeder command : '.$th->getMessage());
            Log::stack([$logChannel])->info('Failed initialize db:seed AdminUserSeeder command : '.$th->getMessage());
        }
        
        Log::stack([$logChannel])->info('Initialize permissions:syncadmin command');
        $this->info('Sync all permissions to admin user');
        $this->call('permissions:syncadmin');
        $this->info('End of install lapdash setup');
    }

    private function installRequiredPackage($logChannel)
    {
        $this->info('Install required packages');
        Log::stack([$logChannel])->info('Install required packages');
        try {
            $this->info('Publish Spatie Permission & ActivityLog');
            $this->call('vendor:publish',['--provider' => 'Spatie\Permission\PermissionServiceProvider']);
            $this->call('vendor:publish',['--provider' => 'Spatie\Activitylog\ActivitylogServiceProvider']);
            $this->info('Laravel session table');
            $this->call('session:table');
        } catch (\Throwable $th) {
            $this->info('Failed laravel session table command : '.$th->getMessage());
            Log::stack([$logChannel])->info('Failedlaravel session table command : '.$th->getMessage());
        }
        
        $this->migrateDB($logChannel);
    }

    private function migrateDB($logChannel)
    {

        try {
            $this->info('Migrate database');
            $this->call('migrate');
        } catch (\Throwable $th) {
            $this->error('Failed migrate database command : '.$th->getMessage());
            Log::stack([$logChannel])->info('Failed migrate database command : '.$th->getMessage());
        } 
    }

    private function replaceRoute($logChannel)
    {        
        Log::stack([$logChannel])->info('Replace routes content');
        file_put_contents(
            base_path('routes/web.php'),
            file_get_contents(__DIR__.'/../routes/web.stub')
        );
    }
}
