<?php

namespace Mushmero\Lapdash\Console;

use Illuminate\Console\Command;
use Jackiedo\DotenvEditor\Facades\DotenvEditor;

class RebuildEnv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lapdash:rebuildenv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('start rebuild env');
        $referrerEnv = $this->processEnv(DotenvEditor::load(base_path('.env.example'))->getEntries(true));
        foreach($referrerEnv as $k => $v){
            if($k == 'APP_NAME' && DotenvEditor::load(base_path('.env'))->keyExists('APP_NAME')){
                DotenvEditor::load(base_path('.env'))->setKey('APP_NAME', $v);
                DotenvEditor::save();
            }
            if($k == 'SESSION_DRIVER' && DotenvEditor::load(base_path('.env'))->keyExists('SESSION_DRIVER')){
                DotenvEditor::load(base_path('.env'))->setKey('SESSION_DRIVER', $v);
                DotenvEditor::save();
            }
            if($k == 'SESSION_LIFETIME' && DotenvEditor::load(base_path('.env'))->keyExists('SESSION_LIFETIME')){
                DotenvEditor::load(base_path('.env'))->setKey('SESSION_LIFETIME', $v);
                DotenvEditor::save();
            }
            if(!DotenvEditor::load(base_path('.env'))->keyExists($k)){
                DotenvEditor::load(base_path('.env'))->setKey($k, $v);
                DotenvEditor::save();
            }
        }
        $this->info('success');
        return Command::SUCCESS;
    }

    private function processEnv($contents){
        $arr = [];
        $result = [];
        foreach($contents as $row){
            foreach($row['parsed_data'] as $key => $value){
                if($row['parsed_data']['type'] != 'empty'){
                    if($key == 'key'){
                        $arr[$key] = $value;
                    }
                    if($key == 'value'){
                        $arr[$key] = $value;
                    }
                }
            }
            $result[$arr['key']] = $arr['value'];
        }
        return $result;
    }
}