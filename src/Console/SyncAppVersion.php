<?php

namespace Mushmero\Lapdash\Console;

use Mushmero\Lapdash\Models\AppVersion;
use Carbon\Carbon;
use Str;

use Illuminate\Console\Command;

class SyncAppVersion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'appversion:sync {--major=} {--minor=} {--build=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update app version to latest';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $major = $this->option('major');
        $minor = $this->option('minor');
        $build = $this->option('build');

        $exist = AppVersion::first();

        if($exist){
            $data['major'] = !empty($major) ? $major : $exist->major;
            $data['minor'] = !empty($minor) ? $minor : $exist->minor;
            $data['build'] = !empty($build) ? $build : $exist->build;
            $data['date'] = Carbon::now('Asia/Kuala_Lumpur')->format('Ymd');
            $data['uuid'] = Str::uuid()->toString();

            $exist->update($data);
            $this->info('success - update app version to '.$data['major'].'.'.$data['minor'].'.'.$data['build']);

        }else{
            $data['major'] = !empty($major) ? $major : 1;
            $data['minor'] = !empty($minor) ? $minor : 0;
            $data['build'] = !empty($build) ? $build : 0;
            $data['date'] = Carbon::now('Asia/Kuala_Lumpur')->format('Ymd');
            $data['uuid'] = Str::uuid()->toString();

            AppVersion::create($data);
            $this->info('success - create app version to '.$data['major'].'.'.$data['minor'].'.'.$data['build']);
        }

        return Command::SUCCESS;
    }
}