<?php

namespace App\Helpers;

use Illuminate\Support\Arr;
use Mushmero\Lapdash\Models\AppVersion;
use Illuminate\Support\Facades\Route;
use Carbon\Carbon;
use Session;

class Helper
{    
	public static function appVer()
	{
		$appsVersion = AppVersion::first();
		if(!empty($appsVersion)){
			$version = $appsVersion->major.'.'.$appsVersion->minor.'.'.$appsVersion->build.'_'.$appsVersion->date.' '.$appsVersion->uuid;
		}else{
			$version = '1.0.0';
		}
		return $version;
	}
	public static function generateRandomString($length = 10, $method = 'mixed') {
		if($method == 'mixed'){
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		}else if($method == 'alphabetOnly'){
			$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		}else if($method == 'alphaUpper'){
			$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		}else if($method == 'alphaLower'){			
			$characters = 'abcdefghijklmnopqrstuvwxyz';
		}
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[random_int(0, $charactersLength - 1)];
		}
		return $randomString;
	}
    public static function getModulesFromMenu()
    {
        $menu = config('adminlte.menu');

        foreach($menu as $m){
            if(isset($m['text']) && isset($m['url'])){
                $modules[] = $m['text'];
            }
            if(isset($m['submenu'])){
                foreach($m['submenu'] as $sub){
                    if(isset($sub['text']) && isset($sub['url'])){
                        $modules[] = $sub['text'];
                    }
                }
            }
        }
        
        return $modules;
    }

    public static function getCurrentRoutePrefix()
    {
        $prefix = explode('/',Request()->route()->getPrefix())[1];
        
        return $prefix;
    }

    public static function getModulesFromRoute()
    {
        $modules = array();
        $routes = Route::getRoutes()->getRoutes();

        foreach($routes as $route){
			if(Arr::exists($route->getAction(),'middleware') && is_array($route->getAction()['middleware'])){
				if ($route->getName() != '' && $route->uri() != '/' && in_array('permissionAuth', $route->getAction()['middleware'])) {
					$modules[] = $route->getName();
				}
			}
        }
        return $modules;
    }

	public static function getSystemModules()
	{
		$systemModules = [
			'adminlte.darkmode.toggle',
			'login',
			'logout',
			'register',
			'password.request',
			'password.email',
			'password.reset',
			'password.update',
			'password.confirm',
		];
		return $systemModules;
	}

    public static function formatDate($date, $timezone = 'Asia/Kuala_Lumpur', $format = 'd-m-Y H:i:s')
    {
		if(is_null($date)) return 'Date empty';
        return Carbon::parse($date)->setTimezone($timezone)->format($format);
    }

    public static function bgcolor()
    {
		$color = array(
			'bg-navy' => 'bg-navy',
			'bg-olive' => 'bg-olive',
			'bg-lime' => 'bg-lime',
			'bg-fuchsia' => 'bg-fuchsia',
			'bg-maroon' => 'bg-maroon',
			'bg-blue' => 'bg-blue',
			'bg-indigo' => 'bg-indigo',
			'bg-purple' => 'bg-purple',
			'bg-pink' => 'bg-pink',
			'bg-red' => 'bg-red',
			'bg-orange' => 'bg-orange',
			'bg-yellow' => 'bg-yellow',
			'bg-green' => 'bg-green',
			'bg-teal' => 'bg-teal',
			'bg-cyan' => 'bg-cyan',
			'bg-white' => 'bg-white',
			'bg-gray' => 'bg-gray',
			'bg-gray-dark' => 'bg-gray-dark',
			'bg-gray-light' => 'bg-gray-light',
			'bg-black' => 'bg-black',
			'bg-success' => 'bg-success',
			'bg-danger' => 'bg-danger',
			'bg-info' => 'bg-info',
			'bg-warning' => 'bg-warning',
			'bg-light' => 'bg-light',
			'bg-dark' => 'bg-dark',
			'bg-primary' => 'bg-primary',
			'bg-secondary' => 'bg-secondary',
		);
		return $color;
    }
    
	public static function textcolor(){
		$textcolor = array(
			'text-white' => 'text-white',
			'text-primary' => 'text-primary',
			'text-secondary' => 'text-secondary',
			'text-success' => 'text-success',
			'text-info' => 'text-info',
			'text-warning' => 'text-warning',
			'text-danger' => 'text-danger',
			'text-light' => 'text-light',
			'text-dark' => 'text-dark',
			'text-body' => 'text-body',
			'text-muted' => 'text-muted',
			'text-black-50' => 'text-black-50',
			'text-white-50' => 'text-white-50',
		);
		return $textcolor;
	}

	public static function gettextcolor($color){
        $lightColor = ['bg-lime','bg-orange','bg-yellow','bg-white','bg-gray-light','bg-warning','bg-light'];

        if(in_array($color, $lightColor)){
				$textcolor = 'text-dark';
		}else{
			$textcolor = 'text-white';
		}	
		return $textcolor;
	}

	public static function urlHasPermission($url = null)
	{
		$authUser = auth()->user();
		$allRoutes = Route::getRoutes()->getRoutesByName();
		$permissionExist = Session::get('userPermissions') ? Session::get('userPermissions') : [];

		foreach($allRoutes as $key => $route){
			if(is_array($route->getAction()['middleware'])){
				if ($route->getName() != '' && $route->uri() != '/' && in_array('permissionAuth', $route->getAction()['middleware'])) {
					if($route->getPrefix() != ''){
						if(explode('/',$route->getPrefix())[1] == $url){
							if(in_array($route->getAction()['as'], $permissionExist)){
								return true;
							}else{
								return false;
							}
						}
					}
				}
			}
		}
		return false;
	}

	public static function generateRandomNumber($length = 4)
	{		
		$result = '';

		for($i = 0; $i < $length; $i++) {
			$result .= mt_rand(0, 9);
		}

		return $result;
	}

    public static function isDimesionArray($arr)
    {
        $newArr = [];
        foreach($arr as $key => $one){
            if(is_array($one)){
                $newArr[$key] = json_encode($one);
            }else{
                $newArr[$key] = $one;
            }
        }
        return $newArr;
    }
}