<?php

namespace Mushmero\Lapdash\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppVersion extends Model
{
    use HasFactory;
    protected $table = 'app_versions';
    
    protected $fillable = [
        'major',
        'minor',
        'build',
        'date',
        'uuid',
    ];
}
