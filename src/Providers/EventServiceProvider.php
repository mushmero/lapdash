<?php

namespace Mushmero\Lapdash\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Auth\Events\Login;
use Mushmero\Lapdash\Listeners\LoginSuccess;
use Illuminate\Auth\Events\Logout;
use Mushmero\Lapdash\Listeners\LogoutSuccess;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        Login::class => [
            LoginSuccess::class,
        ],
        Logout::class => [
            LogoutSuccess::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}