@extends('lapdash::page')

@section(config('adminlte.title'))

@section('content_header')
    <h1 class="m-0 text-dark">{{ __('adminlte::module.roles.action.select-role') }}</h1>
@stop
@section('plugins.Select2', true)

@section('content')
    <div class="row">
        <div class="col-12">
            @include('flash::message')
            <form class="form" action="{{ route('roles.select.store') }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-4"></div>
                    <div class="col-4">
                        <x-adminlte-select2 id="selectRole" name="selectRole" label="{{ __('adminlte::module.roles.action.select') }}" igroup-size="md" :config="$config" enable-old-support>
                            <option/>
                            @foreach ($roles as $role)
                                <option value="{{ $role }}">{{ $role }}</option>
                            @endforeach
                        </x-adminlte-select2>
                        <x-adminlte-button class="btn-flat" type="submit" label="{{ __('adminlte::module.roles.action.choose') }}" theme="success" icon="fas fa-lg fa-check"/>
                    </div>
                    <div class="col-4"></div>
                </div>
            </form>
        </div>
    </div>
@stop