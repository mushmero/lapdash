@extends('lapdash::page')

@section(config('adminlte.title'), 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Permissions Management</h1>
@stop
@section('plugins.Select2', true)

@section('content')
    <div class="row">
        <div class="col-12">
            @include('flash::message')
            <form class="form" action="{{ route('permissions.update',$permission->id) }}" method="post">
                @method('put')
                @csrf
                <x-adminlte-card title="Edit Permission {{ $permission->name }}" theme="default" icon="fas fa-sm fa-user-cog" collapsible>
                    <x-adminlte-select2 id="modules" name="modules" label="Modules" fgroup-class="col-md-6 row" label-class="col-md-2 control-label"  igroup-class="col-md-10" igroup-size="md" :config="$config" required enable-old-support>
                        <option/>
                        @foreach ($modules as $module)
                            <option value="{{ $module }}" {{ $permission->name == $module ? "selected" : "" }}>{{ $module }}</option>
                        @endforeach
                    </x-adminlte-select2>
                    <x-slot name="footerSlot">
                        <a href="{{ route('permissions') }}">
                            <x-adminlte-button class="btn-flat" label="{{ __('adminlte::adminlte.back') }}" theme="default" icon="fas fa-chevron-left"/>
                        </a>
                        <x-adminlte-button class="btn-flat" type="submit" label="Submit" theme="success" icon="fas fa-lg fa-save"/>
                    </x-slot>
                </x-adminlte-card>
            </form>
        </div>
    </div>
@stop