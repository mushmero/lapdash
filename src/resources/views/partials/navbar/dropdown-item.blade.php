@inject('navbarItemHelper', 'Mushmero\Lapdash\Helpers\NavbarItemHelper')

@if ($navbarItemHelper->isSubmenu($item))

    {{-- Dropdown submenu --}}
    @include('lapdash::partials.navbar.dropdown-item-submenu')

@elseif ($navbarItemHelper->isLink($item))

    {{-- Dropdown link --}}
    @include('lapdash::partials.navbar.dropdown-item-link')

@endif
