<li @isset($item['id']) id="{{ $item['id'] }}" @endisset class="nav-header {{ $item['class'] ?? '' }} {{ !$sidebarItemHelper->isSubmenu($item)  ? 'hide' : ''}}">

    {{ is_string($item) ? $item : $item['header'] }}

</li>
