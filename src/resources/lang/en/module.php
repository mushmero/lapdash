<?php

return [
    'user_module'           => 'Users Management',
    'user_list'             => 'Users List',
    'user.name'             => 'Name',
    'user.email'            => 'Email',
    'user.role'             => 'Role',
    'user.member_since'     => 'Member Since',
    'action'                => 'Action',
    'submit'                => 'Submit',
    'roles'          => [
        'moduleName'    => 'Roles',
        'action'    => [
            'select' => 'Select',
            'select-role' => 'Select Role',
            'choose' => 'Choose',
            'create' => 'Create',
        ],
    ],
    
];