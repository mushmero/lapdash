<?php

return [

    'important'                     => 'Important',
    'warning'                       => 'Warning',
    'information'                   => 'Information',
    'settings'                      => 'Settings',
    'settings_admin'                => 'Admin',
    'logs'                          => 'Logs',
    'user_access'                   => 'User Access',
    'settings_users'                => 'Users',
    'settings_roles'                => 'Roles',
    'settings_permissions'          => 'Permissions',
    'home'                          => 'Home',
    'users'                         => 'Users',
    'roles'                         => 'Roles',
    'permissions'                   => 'Permissions',
    'system'                        => 'System',
    'show'                          => 'Show',
    'create'                        => 'Create',
    'edit'                          => 'Edit',
    'update'                        => 'Update',
    'delete'                        => 'Delete',
    'index'                         => 'Index',
    'store'                         => 'Store',
    'restore'                       => 'Restore',
    'preview'                       => 'Preview',
];
