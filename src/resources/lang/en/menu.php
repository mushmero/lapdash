<?php

return [

    'main_navigation'               => 'MAIN NAVIGATION',
    'account_settings'              => 'ACCOUNT SETTINGS',
    'profile'                       => 'Profile',
    'change_password'               => 'Change Password',
    'labels'                        => 'LABELS',
    'important'                     => 'Important',
    'warning'                       => 'Warning',
    'information'                   => 'Information',
    'admin_setting'                 => 'ADMIN SETTINGS',
    'settings'                      => 'Settings',
    'settings_admin'                => 'Admin',
    'logs'                          => 'Logs',
    'user_access'                   => 'User Access',
    'settings_users'                => 'Users',
    'settings_roles'                => 'Roles',
    'settings_permissions'          => 'Permissions',
];
