<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use Auth;
use DB;
use Illuminate\Http\Request;
use Session;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesController extends Controller
{
    protected $btnEdit, $btnDelete, $btnDetails;
    
    function __construct()
    {
        $this->middleware(\App\Http\Middleware\Autologout::class);
        $this->btnEdit = config('lapdash.btn-edit');
        $this->btnDelete = config('lapdash.btn-delete');
        $this->btnDetails = config('lapdash.btn-view');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array();
        $user = Auth::user();
        $roles = Role::all();
        $heads = [
            'Name',
            ['label' => 'Actions', 'no-export' => true, 'width' => 5],
        ];

        foreach($roles as $role){
            $data[] = array(
                $role->name,
                '<nobr><a href="'.url('settings_roles/'.$role->id.'/edit').'">'.$this->btnEdit.'</a><a class="" href="'.url('settings_roles/'.$role->id.'/delete').'">'.$this->btnDelete.'</a><a href="'.url('settings_roles/'.$role->id.'/show').'">'.$this->btnDetails.'</a></nobr>',
            );
        }
        
        $config = [
            'data' => $data,
            'order' => [[0, 'asc']],
            'columns' => [null, null],
        ];

        return view('lapdash::roles.list', [
            'heads' => $heads,
            'config' => $config,
        ]);
    }

    public function selectRoles()
    {
        $roles = [];

        $authUserRoles = auth()->user()->getRoleNames();
        foreach($authUserRoles as $role){
            $roles[] = $role;
        }

        if(Session::has('userPermissions')){
            Session::forget('userPermissions');
        }

        Session::put('userRoles', $roles);

        $config = [
            "placeholder" => __('adminlte::adminlte.select_role'),
            "allowClear" => true,
        ];
        return view('lapdash::roles.select-role',[
            'roles' => $roles,
            'config' => $config,
        ]);
    }

    public function selectRoleStore(Request $request)
    { 
        $this->validate($request, [
            'selectRole' => 'required',
        ]);

        $permitted = [];

        $selectedRole = $request->get('selectRole');
        $permissions = Role::findByName($selectedRole)->permissions;
        $permitted = $permissions->pluck('name')->toArray();

        Session::put('userSelectRole', $selectedRole);
        Session::put('userPermissions', $permitted);

        return redirect()->route('home');
    }
    private function systemModules()
    {
        $systemModules = Helper::getSystemModules();

        $system = [];

        foreach($systemModules as $systemModule){
            $system['system'][] = $systemModule;
        }

        return $system;

    }
    private function customModules()
    {
        $allModules = Helper::getModulesFromRoute();
        $systemModules = Helper::getSystemModules();

        $structuredModules = [];

        foreach(array_diff($allModules, $systemModules) as $module){
            if(strpos($module,'.')){
                $explode = explode('.', $module);
                $structuredModules[$explode[0]][] = $explode[1];
            }else{
                $structuredModules[$module][] = $module;
            }
        }

        return $structuredModules;
    }

    private function getSelectedPermissionsId($permissions)
    {
        $permissionID = [];
        foreach($permissions as $permission){
            $explodePermission = explode('.', $permission);
            if(count($explodePermission) == 2){
                if($explodePermission[0] == $explodePermission[1]){
                    $permissionID[] = Permission::findByName($explodePermission[1]);
                }else{
                    $permissionID[] = Permission::findByName($permission);
                }
            }else{
                $permissionID[] = Permission::findByName($permission);
            }
        }
        return $permissionID;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $role = Role::find($id);
        $permissions = $this->customModules();
        $systemPermissions = $this->systemModules();
        $protectedRoles = $this->protectedRoles;

        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();

        $permittedPermissions = [];
        foreach($rolePermissions as $rolePermission){
            $permittedPermissions[] = Permission::findById($rolePermission)->name;
        }

        return view('lapdash::roles.edit',compact('role','permissions','systemPermissions','permittedPermissions','protectedRoles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'systemPermissions' => 'required',
            'permissions' => 'required',
        ]);

        $permitted = [];

        $selectedPermissions = array_merge($request->input('systemPermissions'), $request->input('permissions'));
        $permissions = $this->getSelectedPermissionsId($selectedPermissions);

        foreach($permissions as $permission){
            $permitted[] = $permission->name;
        }
    
        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();
        $role->syncPermissions($permissions);
        if($request->input('name') == Session::get('userSelectRole')){
            Session::put('userPermissions', $permitted);
        }

        if($role){
            flash(
                'Role updated successfully',
            )->success();
        }else{
            flash(
                'Unable to update role',
            )->error();
        }

         return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = $this->customModules();
        $systemPermissions = $this->systemModules();
        $config = [
            "placeholder" => "Select required permissions",
            "allowClear" => true,
        ];
        return view('lapdash::roles.create',compact('systemPermissions','permissions','config'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'systemPermissions' => 'required',
            'permissions' => 'required',
        ]);
        $selectedPermissions = array_merge($request->input('systemPermissions'), $request->input('permissions'));
        $permissions = $this->getSelectedPermissionsId($selectedPermissions);

        $role = Role::create(['name' => $request->input('name')]);
        
        $role->syncPermissions($permissions);

        if($role){
            flash(
                'Role created successfully',
            )->success();
        }else{
            flash(
                'Unable to create role',
            )->error();
        }

         return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        $permissions = $this->customModules();
        $systemPermissions = $this->systemModules();

        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();

        $permittedPermissions = [];
        foreach($rolePermissions as $rolePermission){
            $permittedPermissions[] = Permission::findById($rolePermission)->name;
        }

        return view('lapdash::roles.show',compact('role','permissions','systemPermissions','permittedPermissions'));
    }
}
