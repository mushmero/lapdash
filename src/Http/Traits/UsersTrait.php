<?php

namespace App\Http\Traits;

use App\Helpers\Helper;
use App\Models\User;
use Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Carbon\Carbon;

trait UsersTrait 
{
    public static function userRole()
    {
        return 'Users';
    }
    public static function systemModules()
    {
        $systemModules = Helper::getSystemModules();

        $system = [];

        foreach($systemModules as $systemModule){
            $system['system'][] = $systemModule;
        }

        return $system;

    }
    public static function customModules()
    {
        $allModules = Helper::getModulesFromRoute();
        $systemModules = Helper::getSystemModules();

        $structuredModules = [];

        foreach(array_diff($allModules, $systemModules) as $module){
            if(strpos($module,'.')){
                $explode = explode('.', $module);
                $structuredModules[$explode[0]][] = $explode[1];
            }else{
                $structuredModules[$module][] = $module;
            }
        }

        return $structuredModules;
    }

    public static function getSelectedPermissionsId($permissions)
    {
        $permissionID = [];
        foreach($permissions as $permission){
            $explodePermission = explode('.', $permission);
            if(count($explodePermission) == 2){
                if($explodePermission[0] == $explodePermission[1]){
                    $permissionID[] = Permission::findByName($explodePermission[1]);
                }else{
                    $permissionID[] = Permission::findByName($permission);
                }
            }else{
                $permissionID[] = Permission::findByName($permission);
            }
        }
        return $permissionID;
    }

    public static function generatePassword($string)
    {
        return Hash::make($string);
    }

    private static function createRoleIfNotExists($role)
    {
        $allRoles = Role::all()->pluck('name')->toArray();
        if(in_array($role, $allRoles)){
            $exist = Role::findByName($role);
            return $exist;
        }else{
            $newRole = Role::create(['name' => $role]);
            return $newRole;
        }
    }

    public static function findUserByEmail($email)
    {
        $exist = User::where(['email' => $email])->first();
        return $exist;
    }

    public static function findUserById($id)
    {
        $exist = User::where(['id' => $id])->first();
        return $exist;
    }

    public static function delete($id)
    {
        $delete = [
            'deleted_at' => Carbon::now(),
            'deleted_by' => auth()->user()->id,
        ];
        User::where('id',$id)->update($delete);
        return true;
    }

    public static function restore($id)
    {
        User::withTrashed()->where('id', $id)->update(['deleted_by' => '']);
        User::withTrashed()->where('id', $id)->restore();
        return true;
    }

}