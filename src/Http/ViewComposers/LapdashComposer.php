<?php

namespace Mushmero\Lapdash\Http\ViewComposers;

use Illuminate\View\View;
use Mushmero\Lapdash\Lapdash;

class LapdashComposer
{
    /**
     * @var Lapdash
     */
    private $lapdash;

    public function __construct(Lapdash $lapdash)
    {
        $this->lapdash = $lapdash;
    }

    public function compose(View $view)
    {
        $view->with('lapdash', $this->lapdash);
    }
}
