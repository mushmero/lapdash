<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Session;
use Spatie\Permission\Exceptions\UnauthorizedException;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission = null, ...$guards)
    {
        $authGuard = app('auth')->guard($guards);

        if ($authGuard->guest()) {
            throw UnauthorizedException::notLoggedIn();
        }

        if (! is_null($permission)) {
            $permissions = is_array($permission)
                ? $permission
                : explode('|', $permission);
        }

        if ( is_null($permission) ) {
            $permission = $request->route()->getName();

            $permissions = array($permission);
        }

        $permissionExist = Session::get('userPermissions') ? Session::get('userPermissions') : [];
        
        foreach($permissions as $permission){
            if(in_array($permission, $permissionExist)){
                return $next($request);
            }else{
                if(!empty($permissionExist) && !in_array($permission, $permissionExist)){
                    flash(
                        __('adminlte::message.unauthorized_permissions'),
                    )->error();
                    return back();
                }
                return redirect()->route('roles.select');
            }
        }

        throw UnauthorizedException::forPermissions($permissions);
    }
}