<?php

namespace Mushmero\Lapdash;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\View\Factory;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Mushmero\Lapdash\Console\GenerateRole;
use Mushmero\Lapdash\Console\GenerateRoutePermissions;
use Mushmero\Lapdash\Console\LapdashDbSetup;
use Mushmero\Lapdash\Console\LapdashSetup;
use Mushmero\Lapdash\Console\SyncAdminPermission;
use Mushmero\Lapdash\Console\SyncAppVersion;
use Mushmero\Lapdash\Console\SyncRoleToUser;
use Mushmero\Lapdash\Console\RebuildEnv;
use Mushmero\Lapdash\Http\Middleware\Autologout;
use Mushmero\Lapdash\Http\Middleware\PermissionMiddleware;
use Mushmero\Lapdash\Http\ViewComposers\LapdashComposer;
use Mushmero\Lapdash\Providers\EventServiceProvider;
use Jackiedo\DotenvEditor\DotenvEditorServiceProvider;
use Spatie\Permission\PermissionServiceProvider;

class LapDashServiceProvider extends BaseServiceProvider
{
    /**
     * The prefix to use for register/load the package resources.
     *
     * @var string
     */
    protected $pkgPrefix = 'lapdash';

    /**
     * Register the package services.
     *
     * @return void
     */
    public function register()
    {
        // Bind a singleton instance of the AdminLte class into the service
        // container.

        $this->app->singleton(Lapdash::class, function (Container $app) {
            return new Lapdash(
                $app,
                $app['config']['adminlte.filters'],
                $app['events']
            );
        });
        $this->app->register(PermissionServiceProvider::class);
        $this->app->register(EventServiceProvider::class);
        $this->app->register(DotenvEditorServiceProvider::class);
    }

    /**
     * Bootstrap the package's services.
     *
     * @return void
     */
    public function boot(Factory $view)
    {
        // $this->loadRoutes();
        // $this->loadMiddleware();
        $this->loadViews();
        $this->registerCommands();
        $this->registerViewComposers($view);
        $this->loadDbSeed(['AdminUserSeeder']);
        $this->loadConfig(['lapdash','adminlte','auth','activitylog']);
        $this->loadAssets();
        $this->loadMigrations();
        $this->loadTranslation();
        $this->loadUserModels();
        $this->publishCustomEnv();
        // $this->publishDefaultHome();
        $this->publishStuffs();
    }

    /**
     * Load the package views.
     *
     * @return void
     */
    private function loadViews()
    {
        $viewsPath = $this->packagePath('resources/views');
        $this->loadViewsFrom($viewsPath, $this->pkgPrefix);
        $this->publishes([
            $viewsPath => base_path('resources/views/vendor/'.$this->pkgPrefix),
            $this->packagePath('resources/views/home.blade.php') => base_path('resources/views/home.blade.php'),
        ], 'lapdash-views');
    }

    /**
     * Register the package's artisan commands.
     *
     * @return void
     */
    private function registerCommands()
    {
        $this->commands([
            GenerateRoutePermissions::class,
            GenerateRole::class,
            SyncAdminPermission::class,
            SyncRoleToUser::class,
            LapdashSetup::class,
            RebuildEnv::class,
            LapdashDbSetup::class,
            SyncAppVersion::class,
        ]);
    }

    /**
     * Register the package's view composers.
     *
     * @return void
     */
    private function registerViewComposers(Factory $view)
    {
        $view->composer('lapdash::page', LapdashComposer::class);
    }

    /**
     * Get the absolute path to some package resource.
     *
     * @param  string  $path  The relative path to the resource
     * @return string
     */
    private function packagePath($path)
    {
        return __DIR__."/$path";
    }

    private function loadDbSeed($seeder = [])
    {
        if(empty($seeder)) return;
        foreach($seeder as $seed){
            if($seed == 'AdminUserSeeder'){
                $seedpath = $this->packagePath('Database/seeders/AdminUserSeeder.php');
            }

            if(!class_exists($seed)){
                $this->publishes([
                    $seedpath => database_path('seeders/'.$seed.'.php'),
                ], 'admin-seeds');
            }

        }
    }

    private function loadConfig($config = [])
    {
        if(empty($config)) return;

        foreach($config as $cfg){
            if($cfg == 'lapdash'){
                $cfgpath = $this->packagePath('config/lapdash.php');
            }else if($cfg == 'adminlte'){
                $cfgpath = $this->packagePath('config/adminlte.php');
            }else if($cfg == 'auth'){
                $cfgpath = $this->packagePath('config/auth.php');
            }else if($cfg == 'activitylog'){
                $cfgpath = $this->packagePath('config/activitylog.php');
            }
            $this->publishes([$cfgpath => config_path($cfg.'.php')], 'lapdash-config');
        }
    }

    private function loadAssets()
    {
        $this->publishes([
            $this->packagePath('resources/assets') => public_path('assets'),
        ], 'lapdash-assets');
    }

    private function loadMiddleware()
    {
        // $router = $this->app->make(Router::class);
        // $router->aliasMiddleware('autologout', Autologout::class);
        // $router->aliasMiddleware('permission', PermissionMiddleware::class);
    }

    private function loadMigrations()
    {
        $this->loadMigrationsFrom($this->packagePath('database/migrations'));
        $this->publishes([
            $this->packagePath('Database/migrations/2023_03_09_115650_create_app_versions_table.php') => database_path('migrations/2023_03_09_115650_create_app_versions_table.php'),
            $this->packagePath('Database/migrations/2023_03_28_121649_add_softdeleter_to_users_table.php') => database_path('migrations/2023_03_28_121649_add_softdeleter_to_users_table.php')
        ],'lapdash-migrations');
    }

    private function loadTranslation()
    {
        $translationsPath = $this->packagePath('resources/lang');
        $this->loadTranslationsFrom($translationsPath, 'adminlte');
        $this->publishes([$this->packagePath('resources/lang') => base_path('lang/vendor/adminlte')], 'lapdash-translations');
    }

    private function loadUserModels()
    {
        $this->publishes([$this->packagePath('Models/User.php') => base_path('app/Models/User.php')], 'lapdash-user-models');
    }

    private function publishCustomEnv()
    {
        $this->publishes([$this->packagePath('env/.env.example') => base_path('.env.example')], 'lapdash-env');
    }

    /**
     * Load the package web routes.
     *
     * @return void
     */
    private function loadRoutes()
    {
        // $routesPath = $this->packagePath('routes/system.php');
        // $this->loadRoutesFrom($routesPath);
    }

    private function publishDefaultHome()
    {
        // $this->publishes([
        //     $this->packagePath('Http/Controllers/HomeController.php') => base_path('app/Http/Controllers/HomeController.php'),
        //     $this->packagePath('resources/views/home.blade.php') => base_path('resources/views/home.blade.php'),
        // ], 'lapdash-home');
    }

    private function publishStuffs()
    {
        $this->publishes([
            $this->packagePath('Http/Controllers/User/PermissionsController.php') => base_path('app/Http/Controllers/User/PermissionsController.php'),
            $this->packagePath('Http/Controllers/User/RolesController.php') => base_path('app/Http/Controllers/User/RolesController.php'),
            $this->packagePath('Http/Controllers/User/UsersController.php') => base_path('app/Http/Controllers/User/UsersController.php'),
            $this->packagePath('Http/Controllers/Auth/LoginController.php') => base_path('app/Http/Controllers/Auth/LoginController.php'),
            $this->packagePath('Http/Controllers/ActivityLogController.php') => base_path('app/Http/Controllers/ActivityLogController.php'),
            $this->packagePath('Helpers/Helper.php') => base_path('app/Helpers/Helper.php'),
            $this->packagePath('Http/Middleware/Autologout.php') => base_path('app/Http/Middleware/Autologout.php'),
            $this->packagePath('Http/Middleware/PermissionMiddleware.php') => base_path('app/Http/Middleware/PermissionMiddleware.php'),
            $this->packagePath('Http/Traits/UsersTrait.php') => base_path('app/Http/Traits/UsersTrait.php'),
            $this->packagePath('resources/lang') => base_path('lang/vendor/adminlte'),
        ], 'lapdash-stuffs');
    }

}